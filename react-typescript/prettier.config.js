module.exports = {
  // eslint-disable-next-line global-require,import/no-extraneous-dependencies
  ...require('@kateinnovations/prettier-config'),
  arrowParens: 'avoid',
  printWidth: 80,
  semi: false,
  singleQuote: true,
};
